import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { productMapping } from '../data/products';


interface ProductAddCallback {
    (productID: number);
}


@Component({
    selector: 'app-add-product-dialog',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">Add product</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group justify-content-center mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="productSelector">Product</label>
        </div>
        <select id="productSelector" [(ngModel)]="productID">
            <option *ngFor="let product of products" [value]="product"> {{ getProductName(product) }}</option>
        </select>
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-success" (click)="addProduct()">Add</button>
        <button class="btn btn-danger" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class AddProductDialogComponent {

    productID: string;
    products: number[];
    callback: ProductAddCallback;

    constructor(
        public bsModalRef: BsModalRef
    ) {
    }

    getProductName(productID: string) {
        return productMapping[parseInt(productID, 10)];
    }

    addProduct() {
        this.callback(parseInt(this.productID, 10));
        this.bsModalRef.hide();
    }
}
