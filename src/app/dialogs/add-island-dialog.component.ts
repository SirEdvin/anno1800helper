import { Component} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Island } from '../data/islands';

interface AddIslandCallback {
    (island: Island);
}


@Component({
    selector: 'app-island-dialog',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">New Island</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="nameInput">Island Name</label>
        </div>
        <input [(ngModel)]="name" id="nameInput">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="islandTypeSelector">Island type</label>
        </div>
        <select id="islandTypeSelector" [(ngModel)]="islandType">
            <option selected value="Old World">Old World</option>
            <option value="New World">New World</option>
        </select>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="houseDeltaInput">House Delta</label>
        </div>
        <input id="houseDeltaInput" [(ngModel)]="houseDelta" type="number">
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-success" color="primary" (click)="addIsland()">Add</button>
        <button class="btn btn-danger" color="warn" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class AddIslandDialogComponent {

    name: string;
    islandType: string;
    houseDelta: number;
    addIslandCallback: AddIslandCallback;

    constructor(
        public bsModalRef: BsModalRef
    ) {
        this.name = 'New island';
        this.islandType = 'Old World';
        this.houseDelta = 10;
    }

    addIsland() {
        const newIsland = new Island(this.name, this.islandType === 'Old World', null, this.houseDelta);
        this.addIslandCallback(newIsland);
        this.bsModalRef.hide();
    }
}
