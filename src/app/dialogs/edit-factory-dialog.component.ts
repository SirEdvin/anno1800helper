import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { Products } from '../data/products';
import { Factory, FactoryState } from '../data/factories';


@Component({
    selector: 'app-edit-factory-dialog',
    templateUrl: './edit-factory-dialog.component.html'
})
export class EditFactoryDialogComponent implements OnInit {

    inputs: number[] = [undefined, undefined, undefined];
    productivity: number;
    state: FactoryState;

    factory: Factory;

    constructor(
        public bsModalRef: BsModalRef
    ) { }


    ngOnInit() {
        this.factory.Inputs.forEach((ingredient, index) => {
            this.inputs[index] = ingredient.ProductID;
        });
        this.productivity = this.factory.Productivity;
        this.state = this.factory.State;
    }

    get products() {
        return Products;
    }

    availableFactoryStates() {
        if (this.factory.State === FactoryState.NORMAL) {
            return [FactoryState.EXTRA, FactoryState.PAUSED, FactoryState.NORMAL];
        }
        return [FactoryState.NORMAL, this.factory.State];
    }

    saveFactory() {
        const oldInputs = this.factory.Inputs;
        this.factory.Inputs = this.inputs.filter(x => x !== undefined).map(x => {
            return {
                AmountPerMinute: this.factory.RealCountPerMinute,
                ProductID: x
            };
        });
        this.factory.notifyInputChange(oldInputs);
        this.factory.Productivity = this.productivity;
        this.factory.State = this.state;
        this.bsModalRef.hide();
    }
}
