import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { Island } from '../data/islands';


@Component({
    selector: 'app-edit-island-dialog',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">Island configuration</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="nameInput">Island Name</label>
        </div>
        <input [(ngModel)]="name" id="nameInput">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="islandTypeSelector">Island type</label>
        </div>
        <select id="islandTypeSelector" [(ngModel)]="islandType">
            <option selected value="Old World">Old World</option>
            <option value="New World">New World</option>
        </select>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="houseDeltaInput">House Delta</label>
        </div>
        <input id="houseDeltaInput" [(ngModel)]="houseDelta" type="number">
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-success" (click)="editIsland()">Save</button>
        <button class="btn btn-danger" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class EditIslandDialogComponent implements OnInit {

    name: string;
    islandType: string;
    houseDelta: number;
    island: Island;

    constructor(
        public bsModalRef: BsModalRef
    ) {
    }

    ngOnInit() {
        this.name = this.island.Name;
        this.islandType = this.island.isOldWorld ? 'Old World' : 'New World';
        this.houseDelta = this.island.houseDelta;
    }

    editIsland() {
        this.island.Name = this.name;
        this.island.isOldWorld = this.islandType === 'Old World';
        this.island.houseDelta = this.houseDelta;
        this.bsModalRef.hide();
    }
}
