import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

interface LoadGameCallback {
    (data);
}

@Component({
    selector: 'app-load-game-dialog',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">Load game</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="fileInput">Game file</label>
        </div>
        <div class="custom-file">
            <input id="fileInput" type="file" class="custom-file-input" aria-describedby="gameFileInput" (change)="onFileChange($event)">
            <label class="custom-file-label" for="fileInput">{{ fileName }}</label>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-warning" (click)="loadGame()">Load</button>
        <button class="btn btn-danger" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class LoadGameDialogComponent {

    gameData: Blob;
    fileName = 'Choose file';
    loadGameCallback: LoadGameCallback;

    constructor(
        public bsModalRef: BsModalRef
    ) { }

    onFileChange(event) {
        this.gameData = event.target.files[0];
        // Awful things required for bootstrap god!
        this.fileName = event.target.files[0].name;
    }

    loadGame() {
        const self = this;
        const reader = new FileReader();
        reader.onload = function () {
            self.loadGameCallback(JSON.parse(reader.result as string));
            self.bsModalRef.hide();
        };
        reader.readAsText(this.gameData);
    }
}
