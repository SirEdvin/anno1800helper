import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-confirm-dialog',
  template: `
<div class="modal-header">
  <h4 class="modal-title pull-left">{{ title }}</h4>
  <button type="button" class="close pull-right" aria-label="Close" (click)="cancel()">
      <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-footer">
  <div class="btn-group">
    <button class="btn btn-warning" (click)="confirm()">Confirm</button>
    <button class="btn btn-danger" (click)="cancel()">Cancel</button>
  </div>
</div>
  `
})
export class ConfirmDialogComponent {

  confirmCallback: () => void;
  cancelCallback: () => void;
  title: string;

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  confirm() {
    if (this.confirmCallback !== undefined) {
      this.confirmCallback();
    }
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.cancelCallback !== undefined) {
      this.cancelCallback();
    }
    this.bsModalRef.hide();
  }
}
