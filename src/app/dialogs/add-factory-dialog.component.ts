import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { Factory, factories, FactoryState } from '../data/factories';


interface FactoryMark {
    name: string;
    value: number;
}

interface FactoryCallback {
    (newFactory: Factory);
}

@Component({
    selector: 'app-add-factory-dialog',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">New Factory</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group justify-content-center mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="factorySelector">Factory type</label>
        </div>
        <select id="factorySelector" [(ngModel)]="factoryID">
            <option *ngFor="let factory of factories" [value]="factory.value"> {{ factory.name }}</option>
        </select>
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-success" (click)="addFactory()">Add</button>
        <button class="btn btn-danger" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class AddFactoryDialogComponent implements OnInit {

    factoryID: string;
    isOldWorldIsland: boolean;
    isOldWorldFactory: boolean;
    factoryCallback: FactoryCallback;

    factories: FactoryMark[];

    constructor(
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
        console.log(this.isOldWorldFactory);
        this.factories = factories.AllFactories.filter(
            x => x.IsOldWorld === this.isOldWorldFactory
        ).map(x => {
            return {
                'name': x.Name,
                'value': x.ID
            };
        }).sort((a, b) => a.name > b.name ? 1 : (a.name === b.name ? 0 : -1));
    }

    addFactory() {
        const factoryID = parseInt(this.factoryID, 10);
        const rawFactory = factories.AllFactories.filter(x => x.ID === factoryID)[0];
        this.factoryCallback(
            new Factory(
                rawFactory,
                (this.isOldWorldFactory === this.isOldWorldIsland) ? FactoryState.NORMAL : FactoryState.EXTRA
            )
        );
        this.bsModalRef.hide();
    }
}
