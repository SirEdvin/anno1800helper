import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { Island } from '../data/islands';


@Component({
    selector: 'app-trade-route-creation',
    template: `
<div class="modal-header">
    <h4 class="modal-title pull-left">Add trade route</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group justify-content-center mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="sourceIslandSelector">Source Island</label>
        </div>
        <select id="sourceIslandSelector" [(ngModel)]="sourceIsland">
            <option *ngFor="let island of islands" [value]="island._id"> {{ island.Name }}</option>
        </select>
    </div>
    <div class="input-group justify-content-center mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="targetIslandSelector">Target Island</label>
        </div>
        <select id="targetIslandSelector" [(ngModel)]="targetIsland">
            <option *ngFor="let island of islands" [value]="island._id"> {{ island.Name }}</option>
        </select>
    </div>
</div>
<div class="modal-footer">
    <div class="btn-group">
        <button class="btn btn-success" (click)="addTradeRoute()">Add</button>
        <button class="btn btn-danger" (click)="bsModalRef.hide()">Cancel</button>
    </div>
</div>
`,
})
export class AddTradeRouteDialogComponent implements OnInit {

    sourceIsland: string;
    targetIsland: string;
    islands: Island[];
    callback: (sourceIsland: number, targetIsland: number) => void;

    constructor(
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
    }

    addTradeRoute() {
        this.callback(parseInt(this.sourceIsland, 10), parseInt(this.targetIsland, 10));
        this.bsModalRef.hide();
    }

}
