import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { IslandComponent } from './island/island.component';

import { AddIslandDialogComponent } from './dialogs/add-island-dialog.component';
import { LoadGameDialogComponent } from './dialogs/load-game-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog.component';
import { EditIslandDialogComponent } from './dialogs/edit-island-dialog.component';
import { AddFactoryDialogComponent } from './dialogs/add-factory-dialog.component';
import { AddProductDialogComponent } from './dialogs/add-product-dialog.component';
import { EditFactoryDialogComponent } from './dialogs/edit-factory-dialog.component';
import { AddTradeRouteDialogComponent } from './dialogs/add-trade-route.component';

import { TabsModule, TooltipModule, ModalModule, ProgressbarModule } from 'ngx-bootstrap';


import { FactoryComponent } from './factory/factory.component';
import { PopulationComponent } from './population/population.component';
import { TradeRouteComponent } from './trade-route/trade-route.component';

@NgModule({
  declarations: [
    AppComponent,
    IslandComponent,
    FactoryComponent,
    AddFactoryDialogComponent,
    PopulationComponent,
    ConfirmDialogComponent,
    AddIslandDialogComponent,
    EditIslandDialogComponent,
    TradeRouteComponent,
    AddTradeRouteDialogComponent,
    EditFactoryDialogComponent,
    LoadGameDialogComponent,
    AddProductDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AddFactoryDialogComponent,
    ConfirmDialogComponent,
    AddIslandDialogComponent,
    EditIslandDialogComponent,
    AddTradeRouteDialogComponent,
    EditFactoryDialogComponent,
    LoadGameDialogComponent,
    AddProductDialogComponent
  ]
})
export class AppModule { }
