import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { saveAs } from 'file-saver';

import { environment } from '../environments/environment';

import { Island, islandFromDict } from './data/islands';
import { TradeRoute, tradeRouteFromDict } from './data/tradeRoutes';
import { db, legacyDB } from './data/storage';

import { ConfirmDialogComponent } from './dialogs/confirm-dialog.component';
import { AddIslandDialogComponent } from './dialogs/add-island-dialog.component';
import { LoadGameDialogComponent } from './dialogs/load-game-dialog.component';
import { AddTradeRouteDialogComponent } from './dialogs/add-trade-route.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'anno1800helper';

  islands: Island[];
  tradeRoutes: TradeRoute[];
  globalBlock = true;

  loadingProgress = 0;
  loadingStep = 'App initializing';
  maxLoadingProgress = 4;

  constructor(private modalService: BsModalService) {
    this.islands = [];
    this.tradeRoutes = [];
  }

  async ngOnInit() {
    if (localStorage.getItem('Base User') !== undefined) {
      legacyDB.query({ '_type': 'island' }).forEach(x => this.islands.push(islandFromDict(x)));
      legacyDB.query({ '_type': 'trade_route' }).map(
        x => tradeRouteFromDict(x, this.islands, this.tradeRoutes)
      ).filter(x => x !== undefined).forEach(x => this.tradeRoutes.push(x));
      localStorage.removeItem('Base User');
    }
    this.loadingStep = 'Opening database';
    this.loadingProgress = 1;
    await db.openDatabase(1, event => {
      event.currentTarget.result.createObjectStore('island', { keyPath: 'id', autoIncrement: true });
      event.currentTarget.result.createObjectStore('tradeRoute', { keyPath: 'id', autoIncrement: true });
    });
    this.loadingStep = 'Loading islands';
    this.loadingProgress = 2;
    const islands = await db.getAll(Island.storeName);
    islands.forEach(x => this.islands.push(islandFromDict(x)));
    this.rearrageIslands();
    this.loadingStep = 'Loading trade routes';
    this.loadingProgress = 3;
    const tradeRoutes = await db.getAll(TradeRoute.storeName);
    tradeRoutes.forEach(x => {
      const newTradeRoute = tradeRouteFromDict(x, this.islands, this.tradeRoutes);
      if (newTradeRoute !== undefined) {
        this.tradeRoutes.push(newTradeRoute);
      }
    });
    this.rearrangeTradeRoutes();
    this.loadingStep = 'Finishing load';
    this.loadingProgress = 4;
    this.globalBlock = false;
  }

  rearrageIslands() {
    this.islands = this.islands.sort(function (x, y) {
      if (x.isOldWorld) {
        if (y.isOldWorld) {
          return 0;
        }
        return -1;
      } else {
        if (y.isOldWorld) {
          return 1;
        }
        return 0;
      }
    });
  }

  rearrangeTradeRoutes() {
    this.tradeRoutes = this.tradeRoutes.sort(function (x, y) {
      return x.RouteRange - y.RouteRange;
    });
  }

  addIsland() {
    const self = this;
    this.modalService.show(AddIslandDialogComponent, {
      initialState: {
        addIslandCallback: function (newIsland: Island) {
          self.islands.push(newIsland);
          this.rearrageIslands();
        }
      }
    });
  }

  createTradeRoute() {
    const self = this;
    this.modalService.show(AddTradeRouteDialogComponent, {
      initialState: {
        islands: this.islands,
        callback: function (sourceIsland: number, targetIsland: number) {
          if (sourceIsland !== targetIsland) {
            if (self.tradeRoutes.filter(x => x.SourceIsland._id === sourceIsland && x.TargetIsland._id === targetIsland)[0] === undefined) {
              self.tradeRoutes.push(new TradeRoute(
                self.islands.filter(x => x._id === sourceIsland)[0],
                self.islands.filter(x => x._id === targetIsland)[0]
              ));
            }
          }
          self.rearrangeTradeRoutes();
        }
      }
    });
  }

  removeTradeRoute(tradeRoute: TradeRoute) {
    tradeRoute.zerofy();
    const index = this.tradeRoutes.indexOf(tradeRoute);
    if (index > -1) {
      this.tradeRoutes.splice(index, 1);
    }
    tradeRoute.delete();
  }

  removeIsland(island: Island) {
    this.tradeRoutes.filter(
      x => x.SourceIsland._id === island._id || x.TargetIsland._id === island._id
    ).forEach(x => this.removeTradeRoute(x));
    const index = this.islands.indexOf(island);
    if (index > -1) {
      this.islands.splice(index, 1);
    }
    island.delete();
  }

  resetGame() {
    this.modalService.show(ConfirmDialogComponent, {
      initialState: {
        title: `Do you really what to reset game?`,
        cancelCallback: () => { },
        confirmCallback: () => db.deleteDatabase().then(_ => window.location.reload())
      }
    });
  }

  saveGame() {
    const baseJSON = JSON.stringify({
      'version': 1,
      'name': 'baseDB',
      'islands': this.islands.map(x => x.to_dict()),
      'tradeRoutes': this.tradeRoutes.map(x => x.to_dict())
    });
    const blob = new Blob([baseJSON], { type: 'application/json' });
    saveAs(blob, 'baseDB.json');
  }

  loadGame() {
    const self = this;
    this.modalService.show(LoadGameDialogComponent, {
      initialState: {
        loadGameCallback: async function (data) {
          await db.clear(Island.storeName);
          await db.clear(TradeRoute.storeName);
          self.islands = [];
          self.tradeRoutes = [];
          data.islands.forEach(x => self.islands.push(islandFromDict(x)));
          data.tradeRoutes.forEach(x => {
            const newTradeRoute = tradeRouteFromDict(x, self.islands, self.tradeRoutes);
            if (newTradeRoute !== undefined) {
              self.tradeRoutes.push(newTradeRoute);
            }
          });
        }
      }
    });
  }

  get version() {
    return environment.version;
  }
}
