import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { ConfirmDialogComponent } from '../dialogs/confirm-dialog.component';
import { AddProductDialogComponent } from '../dialogs/add-product-dialog.component';

import { TradeRoute, ProductAmount } from '../data/tradeRoutes';

import { productMapping } from '../data/products';
import { productImages } from '../data/images/products';


@Component({
  selector: 'app-trade-route',
  templateUrl: './trade-route.component.html',
  styleUrls: ['./trade-route.component.css']
})
export class TradeRouteComponent {

  @Input('tradeRoute') tradeRoute: TradeRoute;
  @Output('removeEvent') removeEvent = new EventEmitter();

  constructor(public modalService: BsModalService) { }

  productImage(productID: number) {
    return productImages[productID];
  }

  productName(productID: number) {
    return productMapping[productID];
  }

  addOne(product: ProductAmount) {
    product.Amount += 1;
  }

  subtractOne(product: ProductAmount) {
    if (product.Amount > 0) {
      product.Amount -= 1;
    } else if (product.Amount === 0) {
      this.tradeRoute.removeProduct(product.productID);
    }
  }

  addProduct() {
    const self = this;
    this.modalService.show(AddProductDialogComponent, {
      initialState: {
        products: this.tradeRoute.SourceIsland.getAvailableProducts(),
        callback: function(productID: number) {
          self.tradeRoute.addProduct(productID, 1);
        }
      }
    });
  }

  deleteTradeRoute() {
    const self = this;
    this.modalService.show(ConfirmDialogComponent, {
      initialState: {
        title: `Do you really what to remove this trade route?`,
        cancelCallback: () => {},
        confirmCallback: () => self.removeEvent.emit()
      }
    });
  }

}
