import { factoriesImages } from './images/factories';

class LegacyFactoryRaw {
  ID: number;
  Name: string;
  CycleTime: number;
  Inputs: FactoryIngredient[];
  Outputs: FactoryIngredient[];
  IsOldWorld: boolean;
  IsNewWorld: boolean;
}

class FactoryRaw extends LegacyFactoryRaw {

  layer = 0;

  constructor(raw: LegacyFactoryRaw) {
    super();
    this.ID = raw.ID;
    this.Name = raw.Name;
    this.CycleTime = raw.CycleTime;
    this.Inputs = raw.Inputs;
    this.Outputs = raw.Outputs;
    this.IsOldWorld = raw.IsOldWorld;
    this.IsNewWorld = raw.IsNewWorld;
  }
}

export class Factories {
  _allFactories: FactoryRaw[];

  _raw: LegacyFactoryRaw[] = [
    { 'ID': 101122, 'Name': 'Dynamite Shop', 'CycleTime': 0, 'Inputs': [], 'Outputs': [], 'IsOldWorld': false, 'IsNewWorld': false },
    {
      'ID': 1010342, 'Name': 'Cigar Factory', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010252, 'AmountPerMinute': 1 },
      { 'ProductID': 1010242, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010259, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101266, 'Name': 'Poncho Darner', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120036, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 120043, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010317, 'Name': 'Sugar Refinery', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010251, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010239, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101271, 'Name': 'Tortilla Maker', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010193, 'AmountPerMinute': 1 },
      { 'ProductID': 120034, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 120035, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101264, 'Name': 'Fried Plantain Kitchen', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120042, 'AmountPerMinute': 1 },
      { 'ProductID': 120041, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 120033, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101252, 'Name': 'Coffee Roaster', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120031, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 120032, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010341, 'Name': 'Chocolate Factory', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010254, 'AmountPerMinute': 1 },
      { 'ProductID': 1010239, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010258, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010340, 'Name': 'Rum Distillery', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 },
      { 'ProductID': 1010251, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010257, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101273, 'Name': 'Bombín Weaver', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010240, 'AmountPerMinute': 1 },
      { 'ProductID': 120044, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 120037, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101415, 'Name': 'Felt Producer', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120036, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 120044, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010318, 'Name': 'Cotton Mill', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010253, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010240, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101262, 'Name': 'Fish Oil Factory', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120042, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010339, 'Name': 'Pearl Farm', 'CycleTime': 90, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010256, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101272, 'Name': 'Alpaca Farm', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120036, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101270, 'Name': 'Corn Farm', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120034, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101263, 'Name': 'Plantain Plantation', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120041, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101251, 'Name': 'Coffee Plantation', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120031, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010333, 'Name': 'Caoutchouc Plantation', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010255, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010332, 'Name': 'Cocoa Plantation', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010254, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010331, 'Name': 'Cotton Plantation', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010253, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010330, 'Name': 'Tobacco Plantation', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010252, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010329, 'Name': 'Sugar Cane Plantation', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010251, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010364, 'Name': 'Members Club', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010355, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010362, 'Name': 'University', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010353, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010361, 'Name': 'Variety Theatre', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010352, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010359, 'Name': 'Church', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010350, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010365, 'Name': 'Bank', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010356, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010360, 'Name': 'School', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010351, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010358, 'Name': 'Pub', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010349, 'AmountPerMinute': 0 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 101250, 'Name': 'Spectacle Factory', 'CycleTime': 90, 'Inputs': [{ 'ProductID': 1010241, 'AmountPerMinute': 1 },
      { 'ProductID': 1010204, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 120030, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010328, 'Name': 'Jewellers', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010256, 'AmountPerMinute': 1 },
      { 'ProductID': 1010249, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010250, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010327, 'Name': 'Goldsmiths', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010226, 'AmountPerMinute': 1 },
      { 'ProductID': 1010233, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010249, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010326, 'Name': 'Gramophone Factory', 'CycleTime': 120, 'Inputs': [{ 'ProductID': 1010242, 'AmountPerMinute': 1 },
      { 'ProductID': 1010204, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010248, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010284, 'Name': 'Sewing Machine Factory', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 },
      { 'ProductID': 1010219, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010206, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010324, 'Name': 'Clockmakers', 'CycleTime': 90, 'Inputs': [{ 'ProductID': 1010241, 'AmountPerMinute': 1 },
      { 'ProductID': 1010249, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010246, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010323, 'Name': 'Bicycle Factory', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010255, 'AmountPerMinute': 1 },
      { 'ProductID': 1010219, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010245, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010321, 'Name': 'Filament Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010226, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010243, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010320, 'Name': 'Marquetry Workshop', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010242, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010319, 'Name': 'Glassmakers', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010228, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010241, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010300, 'Name': 'Dynamite Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010234, 'AmountPerMinute': 1 },
      { 'ProductID': 1010232, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010222, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010315, 'Name': 'Framework Knitters', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010197, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010237, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010314, 'Name': 'Malthouse', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010192, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010236, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010313, 'Name': 'Flour Mill', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010192, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010235, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010312, 'Name': 'Rendering Works', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010199, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010234, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010311, 'Name': 'Gold Mine', 'CycleTime': 150, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010233, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010309, 'Name': 'Limestone Quarry', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010231, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010308, 'Name': 'Copper Mine', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010230, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010307, 'Name': 'Zinc Mine', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010229, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010305, 'Name': 'Iron Mine', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010227, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010304, 'Name': 'Coal Mine', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010226, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 101331, 'Name': 'Oil Refinery', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010566, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010282, 'Name': 'Brass Smeltery', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010229, 'AmountPerMinute': 1 },
      { 'ProductID': 1010230, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010204, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010303, 'Name': 'Cab Assembly Line', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010211, 'AmountPerMinute': 1 },
      { 'ProductID': 1010224, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010225, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010302, 'Name': 'Motor Assembly Line', 'CycleTime': 90, 'Inputs': [{ 'ProductID': 1010219, 'AmountPerMinute': 1 },
      { 'ProductID': 1010204, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010224, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010301, 'Name': 'Heavy Weapons Factory', 'CycleTime': 120, 'Inputs': [{ 'ProductID': 1010219, 'AmountPerMinute': 1 },
      { 'ProductID': 1010222, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010223, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010299, 'Name': 'Weapon Factory', 'CycleTime': 90, 'Inputs': [{ 'ProductID': 1010219, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010221, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010297, 'Name': 'Furnace', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010227, 'AmountPerMinute': 1 },
      { 'ProductID': 1010226, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010219, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010296, 'Name': 'Steelworks', 'CycleTime': 45, 'Inputs': [{ 'ProductID': 1010219, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010218, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 100659, 'Name': 'Champagne Cellar', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 120014, 'AmountPerMinute': 1 },
      { 'ProductID': 1010241, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 120016, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010316, 'Name': 'Butcher\'s', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010199, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010238, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010294, 'Name': 'Schnapps Distillery', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010195, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010216, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010295, 'Name': 'Cannery', 'CycleTime': 90, 'Inputs': [{ 'ProductID': 1010227, 'AmountPerMinute': 1 },
      { 'ProductID': 1010215, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010217, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010293, 'Name': 'Artisanal Kitchen', 'CycleTime': 120, 'Inputs': [{ 'ProductID': 1010193, 'AmountPerMinute': 1 },
      { 'ProductID': 1010198, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010215, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010292, 'Name': 'Brewery', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010194, 'AmountPerMinute': 1 },
      { 'ProductID': 1010236, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010214, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010291, 'Name': 'Bakery', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010235, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010213, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010289, 'Name': 'Coachmakers', 'CycleTime': 120, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 },
      { 'ProductID': 1010255, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010211, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 100416, 'Name': 'Clay Pit', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010201, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010288, 'Name': 'Sailmakers', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010197, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010210, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010285, 'Name': 'Window-Makers', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 },
      { 'ProductID': 1010241, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010207, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010286, 'Name': 'Light Bulb Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010241, 'AmountPerMinute': 1 },
      { 'ProductID': 1010243, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010208, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010325, 'Name': 'Fur Dealer', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010209, 'AmountPerMinute': 1 },
      { 'ProductID': 1010240, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010247, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 100451, 'Name': 'Sawmill', 'CycleTime': 15, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010196, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010283, 'Name': 'Brick Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010201, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010205, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010281, 'Name': 'Soap Factory', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010234, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010203, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010280, 'Name': 'Concrete Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010231, 'AmountPerMinute': 1 },
      { 'ProductID': 1010219, 'AmountPerMinute': 1 }], 'Outputs': [{ 'ProductID': 1010202, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010560, 'Name': 'Sand Mine', 'CycleTime': 0, 'Inputs': [], 'Outputs': [{ 'ProductID': 1010228, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010310, 'Name': 'Saltpeter Works', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010232, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010278, 'Name': 'Fishery', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010200, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 100655, 'Name': 'Vineyard', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120014, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 100654, 'Name': 'Red Pepper Farm', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010198, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010558, 'Name': 'Hunting Cabin', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010209, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010269, 'Name': 'Pig Farm', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010199, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010267, 'Name': 'Sheep Farm', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010197, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010266, 'Name': 'Lumberjack\'s Hut', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010265, 'Name': 'Potato Farm', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010195, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010264, 'Name': 'Hop Farm', 'CycleTime': 90, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010194, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010263, 'Name': 'Cattle Farm', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010193, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 1010262, 'Name': 'Grain Farm', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010192, 'AmountPerMinute': 1 }], 'IsOldWorld': true, 'IsNewWorld': false
    },
    {
      'ID': 101259, 'Name': 'Pub', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010349, 'AmountPerMinute': 0 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101258, 'Name': 'Church', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010350, 'AmountPerMinute': 0 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101296, 'Name': 'Marquetry Workshop', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010242, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101311, 'Name': 'Gold Mine', 'CycleTime': 150, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010233, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 1010561, 'Name': 'Oil Refinery', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010566, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101268, 'Name': 'Brick Factory', 'CycleTime': 60, 'Inputs': [{ 'ProductID': 1010201, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010205, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101267, 'Name': 'Clay Pit', 'CycleTime': 0, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010201, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101265, 'Name': 'Sailmakers', 'CycleTime': 0, 'Inputs': [{ 'ProductID': 1010197, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010210, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101261, 'Name': 'Sawmill', 'CycleTime': 15, 'Inputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }],
      'Outputs': [{ 'ProductID': 1010196, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101303, 'Name': 'Saltpeter Works', 'CycleTime': 120, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010232, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101269, 'Name': 'Cattle Farm', 'CycleTime': 60, 'Inputs': [],
      'Outputs': [{ 'ProductID': 1010193, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    },
    {
      'ID': 101260, 'Name': 'Lumberjack\'s Hut', 'CycleTime': 15, 'Inputs': [],
      'Outputs': [{ 'ProductID': 120008, 'AmountPerMinute': 1 }], 'IsOldWorld': false, 'IsNewWorld': true
    }].filter(x => x.Outputs.length !== 0 && x.Outputs[0].AmountPerMinute > 0);

  calculateLayer(rawFactory: LegacyFactoryRaw): number {
    if (rawFactory.Inputs.length === 0) {
      return 0;
    } else {
      return rawFactory.Inputs.map(ingredient => {
        const availableFactory = this._raw.filter(x => x.Outputs[0].ProductID === ingredient.ProductID)[0];
        return this.calculateLayer(availableFactory) + 1;
      }).reduce((prev, current) => Math.max(prev, current));
    }
  }

  get AllFactories(): FactoryRaw[] {
    if (!this._allFactories) {
      this._allFactories = this._raw.map(x => {
        const newFactory = new FactoryRaw(x);
        newFactory.layer = this.calculateLayer(x);
        if (newFactory.CycleTime <= 0) {
          newFactory.CycleTime = 30;
        }
        return newFactory;
      });
    }

    return this._allFactories;
  }
}

export const factories = new Factories();


export interface FactoryEvent {
  (factory: Factory, oldRealCountPerMinute: number);
}


export interface FactoryInputChangeEvent {
  (factory: Factory, oldInputs: FactoryIngredient[]);
}


export enum FactoryState {
  NORMAL = 'NORMAL',
  PAUSED = 'PAUSED',
  TRADE = 'TRADE',
  TEMPORARY = 'TEMPORARY',
  EXTRA = 'EXTRA'
}

const FactoryStateColors = {};
FactoryStateColors[FactoryState.NORMAL] = 'white';
FactoryStateColors[FactoryState.PAUSED] = 'lightgray';
FactoryStateColors[FactoryState.TRADE] = 'lightblue';
FactoryStateColors[FactoryState.TEMPORARY] = 'lightgray';
FactoryStateColors[FactoryState.EXTRA] = 'lightgreen';


export class Factory extends FactoryRaw {

  factoryEvent: FactoryEvent;

  factoryInputChangeEvent: FactoryInputChangeEvent;

  _enabled = false;
  _count = 1;
  _productivity = 100;
  _real_count = 1;
  _state: FactoryState;

  __satisfited = false;
  __left_count = 1;

  constructor(raw: FactoryRaw, state: FactoryState = FactoryState.NORMAL) {
    super(raw);
    this.layer = raw.layer;
    this._state = state;
    if (state === FactoryState.TEMPORARY) {
      this.Count = 0;
    }
    this.recalculateRealCount();
  }

  get Image() {
    return factoriesImages[this.ID];
  }

  get Count(): number {
    return this._count;
  }

  set Count(newVal: number) {
    this._count = newVal;
    if (this._count > 0 && this._state === FactoryState.TEMPORARY) {
      this._state = FactoryState.NORMAL;
    }
    this.recalculateRealCount();
  }

  get Productivity(): number {
    return this._productivity;
  }

  set Productivity(newVal: number) {
    this._productivity = newVal;
    this.recalculateRealCount();
  }

  get State(): FactoryState {
    return this._state;
  }

  set State(val: FactoryState) {
    if (this.State === FactoryState.PAUSED) {
      if (val !== this.State) {
        this.unfreeze();
      }
    } else if (this.State === FactoryState.EXTRA) {
      if (val !== this.State) {
        this.makeNormal();
      }
    } else {
      if (val === FactoryState.EXTRA) {
        this.makeExtra();
      } else if (val === FactoryState.PAUSED) {
        this.freeze();
      }
    }
  }

  get Satisfited(): boolean {
    return this.__satisfited;
  }

  set Satisfited(newVal: boolean) {
    this.__satisfited = newVal;
  }

  get cycleFactor() {
    return 60.0 / this.CycleTime;
  }

  recalculateRealCount() {
    // console.debug('Recalculate real count for factory', this.Name);
    const old_real_count = this._real_count;
    const cycleFactor = this.cycleFactor;
    const oldRealCountPerMinute = old_real_count * cycleFactor;
    this._real_count = this._count * (this._productivity / 100.0);
    const realCountPerMinute = cycleFactor * this._real_count;
    if (this._state === FactoryState.PAUSED) {
      this._real_count = 0;
    }
    this.__left_count += this._real_count - old_real_count;
    this.Outputs.forEach(ingredient => ingredient.AmountPerMinute = realCountPerMinute);
    const inputRequired = this._state === FactoryState.NORMAL;
    // console.debug('For this factory (', this.Name, '), input required is', inputRequired);
    this.Inputs.forEach(ingredient => ingredient.AmountPerMinute = inputRequired ? realCountPerMinute : 0);
    // console.debug('Factory required inputes per minute', this.Inputs);
    if (this.factoryEvent !== undefined) {
      this.factoryEvent(this, oldRealCountPerMinute);
    }
  }

  notifyInputChange(previousInputs: FactoryIngredient[]) {
    if (this.factoryInputChangeEvent !== undefined) {
      this.factoryInputChangeEvent(this, previousInputs);
    }
  }

  get RealCount(): number {
    return this._real_count;
  }

  get RealCountPerMinute(): number {
    return this._real_count * this.cycleFactor;
  }

  get LeftCount(): number {
    return this.__left_count;
  }

  get UsedCount(): number {
    return this._real_count - this.__left_count;
  }

  set UsedCount(newVal: number) {
    this.__left_count = this._real_count - newVal;
  }

  get UsedCountPerMinute(): number {
    return (this._real_count - this.__left_count) * this.cycleFactor;
  }

  set UsedCountPerMinute(newVal: number) {
    this.__left_count = this._real_count - newVal / this.cycleFactor;
  }

  get Enabled(): boolean {
    return this._enabled;
  }

  set Enabled(newVal: boolean) {
    this._enabled = newVal;
  }

  get Demand(): FactoryIngredient[] {
    return this.Inputs;
  }

  get Supply(): FactoryIngredient[] {
    return this.Outputs;
  }

  get ButtonStyle(): string {
    if (this.LeftCount > 0) {
      return 'btn-success';
    } else if (this.LeftCount === 0) {
      return 'btn-warning';
    } else {
      return 'btn-danger';
    }
  }

  getCardStyle(): object {
    return {
      'background-color': FactoryStateColors[this._state]
    };
  }

  freeze() {
    if (this._state === FactoryState.NORMAL) {
      this._state = FactoryState.PAUSED;
      this.recalculateRealCount();
    }
  }

  unfreeze() {
    if (this._state === FactoryState.PAUSED) {
      this._state = FactoryState.NORMAL;
      this.recalculateRealCount();
    }
  }

  makeExtra() {
    if (this._state === FactoryState.NORMAL) {
      this._state = FactoryState.EXTRA;
      this.recalculateRealCount();
    }
  }

  makeNormal() {
    if (this._state === FactoryState.EXTRA) {
      this._state = FactoryState.NORMAL;
      this.recalculateRealCount();
    }
  }

  to_dict() {
    const result = {};
    Object.keys(this).filter(x => x.startsWith('_') && !x.startsWith('__')).forEach(x => result[x] = this[x]);
    Object.keys(factories.AllFactories[0]).forEach(x => result[x] = this[x]);
    return result;
  }
}

export function factoryFromDict(dict) {
  const newFactory = new Factory(dict, dict._state);
  Object.keys(dict).forEach(x => newFactory[x] = dict[x]);
  newFactory.recalculateRealCount();
  return newFactory;
}

export class FactoryIngredient {
  ProductID: number;
  AmountPerMinute: number;
}

export class FactorySaveInfo {
  FactoryID: number;
  ParentFactoryID?: number;
  Enabled: boolean;
  BuiltCount: number;
  Productivity: number;
  TradeBalance: number;
}
