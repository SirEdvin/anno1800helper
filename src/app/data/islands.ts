import { Factory, FactoryEvent, factories, factoryFromDict, FactoryState, FactoryInputChangeEvent, FactoryIngredient } from './factories';
import {
  PopulationLevel, OldWorldPopulation,
  NewWorldPopulation,
  PopulationRequirementChanges, PopulationRequirementsChangeCallback
} from './populations';

import { DatabaseModel } from './storage';
import { productMapping } from './products';

class ProductBalance {
  productID: number;
  supply: number;
  demand: number;

  constructor(productID: number) {
    this.productID = productID;
    this.supply = 0;
    this.demand = 0;
  }

  get banalce() {
    return this.supply - this.demand;
  }
}

interface ProductSheet {
  [productID: number]: ProductBalance;
}


export class Island extends DatabaseModel {
  _name: string;
  _population_hided = false;
  _house_delta = 10;

  factories: Factory[];
  balance: ProductSheet;
  isOldWorld: boolean;

  populations: PopulationLevel[];

  factoryRecalculationCallback: FactoryEvent;
  factoryInputChangeCallback: FactoryInputChangeEvent;
  populationRequirementsChangeCallback: PopulationRequirementsChangeCallback;

  constructor(name: string, isOldWorld: boolean, id: number = null, houseDelta: number = null) {
    super();
    this._name = name;
    this.isOldWorld = isOldWorld;
    this.balance = {};
    this.factories = [];

    if (id !== null) {
      this._id = id;
    }

    if (houseDelta !== undefined && houseDelta !== null) {
      this._house_delta = houseDelta;
    }

    const self = this;
    this.factoryRecalculationCallback = function (factory: Factory, old_real_count: number) {
      self.factoryRecalculation(factory, old_real_count);
    };
    this.factoryInputChangeCallback = function(factory: Factory, oldInputs: FactoryIngredient[]) {
      self.factoryInputChange(factory, oldInputs);
    };
    this.populationRequirementsChangeCallback = function (changes: PopulationRequirementChanges) {
      self.populationRecalculation(changes);
    };

    if (this.isOldWorld) {
      this.populations = OldWorldPopulation.map(x => new PopulationLevel(x, this.populationRequirementsChangeCallback));
    } else {
      this.populations = NewWorldPopulation.map(x => new PopulationLevel(x, this.populationRequirementsChangeCallback));
    }
    this.populations.forEach((x, index) => {
      if (this.populations[index + 1] !== undefined) {
        x.promotionTarget = this.populations[index + 1];
      }
      if (this.populations[index - 1] !== undefined) {
        x.promotionSource = this.populations[index - 1];
      }
    });
    this.save();
  }

  get maxLayer() {
    return this.factories.reduce((prev, next) => Math.max(prev, next.layer), 0);
  }

  get layerIndexes() {
    return Array(this.maxLayer + 1).fill(0).map((_, i) => i);
  }

  get populationHided(): boolean {
    return this._population_hided;
  }

  set populationHided(val: boolean) {
    this._population_hided = val;
    this.save();
  }

  get houseDelta(): number {
    return this._house_delta;
  }

  set houseDelta(val: number) {
    this._house_delta = val;
    this.save();
  }

  layerForIndex(layer: number) {
    return this.factories.filter(x => x.layer === layer);
  }

  addFactory(factory: Factory) {
    this.factories.push(factory);
    factory.factoryEvent = this.factoryRecalculationCallback;
    factory.factoryInputChangeEvent = this.factoryInputChangeCallback;
    this.factoryRecalculation(factory, 0);
  }

  removeFactory(factory: Factory) {
    const factoryIndex = this.factories.indexOf(factory);
    if (factoryIndex > -1) {
      factory.freeze();
      this.factories.splice(factoryIndex, 1);
    }
  }

  factoryInputChange(factory: Factory, oldInputs: FactoryIngredient[]) {
    oldInputs.map(ingredient => {
      this.balance[ingredient.ProductID].demand -= ingredient.AmountPerMinute;
      return this.recalculateBalance(ingredient.ProductID);
    });
    factory.Inputs.map(ingredient => {
      if (this.balance[ingredient.ProductID] === undefined) {
        this.balance[ingredient.ProductID] = new ProductBalance(ingredient.ProductID);
      }
      this.balance[ingredient.ProductID].demand += ingredient.AmountPerMinute;
      return this.recalculateBalance(ingredient.ProductID);
    });
  }

  fakeSupply(productID: number, amount: number, previous_amount: number = 0) {
    let selectedFactory = this.factories.filter(x => x.State === FactoryState.TRADE && x.Outputs[0].ProductID === productID)[0];
    if (selectedFactory === undefined) {
      const rawFactory = factories.AllFactories.filter(x => x.Outputs[0].ProductID === productID)[0];
      if (rawFactory === undefined) {
        return;
      }
      selectedFactory = new Factory(rawFactory, FactoryState.TRADE);
      selectedFactory.Count = 0;
      this.addFactory(selectedFactory);
    }
    selectedFactory.Count += amount - previous_amount;
    if (selectedFactory.Count === 0) {
      this.removeFactory(selectedFactory);
    }
  }

  fakeDemand(productID: number, amount: number, previous_amount: number = 0) {
    if (previous_amount === undefined) {
      previous_amount = 0;
    }
    const cycleFactor = 60.0 / factories.AllFactories.filter(
      x => x.Outputs[0].ProductID === productID
    )[0].CycleTime;
    if (this.balance[productID] === undefined) {
      this.balance[productID] = new ProductBalance(productID);
    }
    this.balance[productID].demand += (amount - previous_amount) * cycleFactor;
    this.recalculateBalance(productID);
  }

  get Name(): string {
    return this._name;
  }

  set Name(val: string) {
    this._name = val;
  }

  recalculateBalance(productID: number) {
    // console.debug('Recalculate balance for product', productMapping[productID]);
    let restDemand = this.balance[productID].demand;
    let availableFactories = this.factories.filter(
      x => x.Outputs[0].ProductID === productID && x.State !== FactoryState.PAUSED
    );
    if (availableFactories.length === 0 && restDemand > 0) {
      // console.debug('Because factory for this product is not exists create new one');
      const sourceFactory = factories.AllFactories.filter(
        x => x.Outputs[0].ProductID === productID
      )[0];
      if (sourceFactory !== undefined) {
        const temporaryFactory = new Factory(sourceFactory, FactoryState.TEMPORARY);
        this.addFactory(temporaryFactory);
        availableFactories = [temporaryFactory];
      }
    }
    if (availableFactories.length > 0) {
      const templateFactories = availableFactories.filter(x => x.State === FactoryState.TEMPORARY);
      if (templateFactories.length > 0) {
        const factoryWithoutTemprorary = availableFactories.filter(x => x.State !== FactoryState.TEMPORARY);
        if (factoryWithoutTemprorary.length > 0 || restDemand === 0) {
          // console.debug('Because we can, drop temporary factories');
          templateFactories.forEach(x => this.removeFactory(x));
          availableFactories = factoryWithoutTemprorary;
        }
      }
      availableFactories.forEach(factory => {
        if (restDemand > 0) {
          const availableDemand = Math.min(restDemand, factory.RealCountPerMinute);
          // console.debug('From factory', factory.Name, 'extract', availableDemand);
          factory.UsedCountPerMinute = availableDemand;
          restDemand -= availableDemand;
        } else {
          factory.UsedCountPerMinute = 0;
        }
      });
      if (restDemand > 0) {
        const lastFactory = availableFactories.filter(x => !(x.State === FactoryState.PAUSED)).reverse()[0];
        if (lastFactory !== undefined) {
          // console.debug('From factory', lastFactory.Name, 'extract left demand', restDemand);
          lastFactory.UsedCountPerMinute += restDemand;
          // console.debug('Last factory left count is', lastFactory.LeftCount);
        }
      }
    }
    this.save();
  }

  factoryRecalculation(factory: Factory, oldRealCountPerMinute: number) {
    // console.debug('Recalculate factory', factory.Name, 'for island', this.Name);
    factory.Outputs.forEach(ingredient => {
      if (this.balance[ingredient.ProductID] === undefined) {
        this.balance[ingredient.ProductID] = new ProductBalance(ingredient.ProductID);
      } else {
        this.balance[ingredient.ProductID].supply -= oldRealCountPerMinute;
      }
      this.balance[ingredient.ProductID].supply += ingredient.AmountPerMinute;
      this.recalculateBalance(ingredient.ProductID);
    });
    factory.Inputs.map(ingredient => {
      if (this.balance[ingredient.ProductID] === undefined) {
        this.balance[ingredient.ProductID] = new ProductBalance(ingredient.ProductID);
      } else {
        this.balance[ingredient.ProductID].demand -= oldRealCountPerMinute;
      }
      this.balance[ingredient.ProductID].demand += ingredient.AmountPerMinute;
      // console.debug('Require input', productMapping[ingredient.ProductID], 'in amount per minute', ingredient.AmountPerMinute);
      return this.recalculateBalance(ingredient.ProductID);
    });
  }

  populationRecalculation(changes: PopulationRequirementChanges) {
    return Object.keys(changes).map(x => parseInt(x, 10)).map(productID => {
      if (this.balance[productID] === undefined) {
        this.balance[productID] = new ProductBalance(productID);
      } else {
        this.balance[productID].demand -= changes[productID].old;
      }
      this.balance[productID].demand += changes[productID].new_;
      return this.recalculateBalance(productID);
    });
  }

  to_dict() {
    const baseDict = {
      '_version': 1,
      '_name': this._name,
      '_is_old_world': this.isOldWorld,
      '_population_hided': this._population_hided,
      '_house_delta': this._house_delta,
      '_factories': this.factories.filter(
        x => x.State !== FactoryState.TRADE && x.State !== FactoryState.TEMPORARY
      ).map(x => x.to_dict()),
    };
    if (this.populations !== undefined) {
      baseDict['_population'] = this.populations.map(x => x.HouseCount);
    }
    if (this._id !== undefined) {
      baseDict['id'] = this._id;
    }
    return baseDict;
  }

  get storeName(): string {
    return 'island';
  }

  static get storeName(): string {
    return 'island';
  }

  getAvailableProducts(): number[] {
    return Object.values(this.balance).filter(x => x.banalce > 0).map(x => x.productID);
  }
}

export function islandFromDict(dict): Island {
  const island = new Island(dict._name, dict._is_old_world, dict.id, dict._house_delta);
  island._population_hided = dict._population_hided;
  island.factories = dict._factories.map(x => {
    const factory = factoryFromDict(x);
    factory.factoryEvent = island.factoryRecalculationCallback;
    factory.factoryInputChangeEvent = island.factoryInputChangeCallback;
    return factory;
  });
  island.factories.forEach(x => island.factoryRecalculation(x, 0));
  if (dict._population !== undefined) {
    dict._population.forEach((element, index) => {
      island.populations[index].HouseCount = element;
    });
  }
  return island;
}
