import { factories } from './factories';
import { Island } from './islands';

import { DatabaseModel, db } from './storage';


export const oldWorldProducts = factories.AllFactories.filter(x => x.IsOldWorld).map(x => x.Outputs[0].ProductID);
export const newWorldProducts = factories.AllFactories.filter(x => x.IsNewWorld).map(x => x.Outputs[0].ProductID);

interface RecalculateInterface {
    recalculate: (productID: number, prev_amount: number, new_amount: number) => void;
}

export class ProductAmount {
    _product_id: number;
    _amount: number;
    private recalcualteObject: RecalculateInterface;

    constructor(productID: number, Amount: number, recalculateCallback: RecalculateInterface) {
        this._product_id = productID;
        this._amount = Amount;
        this.recalcualteObject = recalculateCallback;
    }

    get productID(): number {
        return this._product_id;
    }

    get Amount(): number {
        return this._amount;
    }

    set Amount(val: number) {
        const oldAmount = this._amount;
        this._amount = val;
        this.recalcualteObject.recalculate(this._product_id, oldAmount, val);
    }
}

export class TradeRoute extends DatabaseModel implements RecalculateInterface {

    private _source_island: Island;
    private _target_island: Island;
    private _products: ProductAmount[];

    static deleteById(id) {
        db.delete(this.storeName, id).catch(_ => {});
    }

    constructor(sourceIsland: Island, targetIsland: Island, id: number = null) {
        super();
        if (id !== null && id !== undefined) {
          this._id = id;
        }
        this._source_island = sourceIsland;
        this._target_island = targetIsland;
        this._products = [];
        this.save();
    }

    addProduct(productID: number, amount: number) {
        if (this._products.filter(x => x.productID === productID)[0]) {
            const amountObj = this._products.filter(x => x.productID === productID)[0];
            const oldAmount = amountObj.Amount;
            amountObj.Amount += amount;
            this.recalculate(productID, oldAmount, amountObj.Amount);
        } else {
            this._products.push(new ProductAmount(
                productID, amount, this
            ));
            this.recalculate(productID, 0, amount);
        }
    }

    removeProduct(productID: number) {
        if (this._products.filter(x => x.productID === productID)[0]) {
            const amountObj = this._products.filter(x => x.productID === productID)[0];
            const oldAmount = amountObj.Amount;
            amountObj.Amount = 0;
            const index = this._products.indexOf(amountObj);
            if (index > -1) {
                this._products.splice(index, 1);
            }
            this.recalculate(productID, oldAmount, 0);
        }
    }

    get Products(): ProductAmount[] {
        return this._products;
    }

    get SourceIsland(): Island {
        return this._source_island;
    }

    get TargetIsland(): Island {
        return this._target_island;
    }

    get RouteRange(): number {
      if (this._source_island.isOldWorld) {
        if (this._target_island.isOldWorld) {
          return 1;
        }
        return 3;
      } else {
        if (this._target_island.isOldWorld) {
          return 3;
        }
        return 2;
      }
    }

    recalculate(productID: number, prev_amount: number, new_amount: number) {
        this._source_island.fakeDemand(productID, new_amount, prev_amount);
        this._target_island.fakeSupply(productID, new_amount, prev_amount);
        this.save();
    }

    zerofy() {
        this._products.forEach(x => {
            const oldAmount = x.Amount;
            x.Amount = 0;
            this.recalculate(x.productID, oldAmount, 0);
        });
    }

    to_dict() {
        const baseDict = {
            '_version': 2,
            '_source_island': this._source_island._id,
            '_target_island': this._target_island._id,
            '_products': this._products.map(x => {
                return {
                    productID: x.productID,
                    Amount: x.Amount
                };
            }),
        };
        if (this._id !== undefined) {
            baseDict['id'] = this._id;
        }
        return baseDict;
    }

    get storeName(): string {
        return 'tradeRoute';
    }

    static get storeName(): string {
        return 'tradeRoute';
    }

}

export function tradeRouteFromDict(dict, islands: Island[], tradeRoutes: TradeRoute[]) {
    const sourceIsland = islands.filter(x => x._id === dict._source_island)[0];
    const targetIsland = islands.filter(x => x._id === dict._target_island)[0];
    if (sourceIsland === undefined || targetIsland === undefined) {
        return undefined;
    }
    // Search in previous trade routes
    let newTradeRoute = tradeRoutes.filter(
        x => x.SourceIsland._id === dict._source_island && x.TargetIsland._id === dict._target_island
    )[0];
    let realReturn = true;
    if (newTradeRoute === undefined) {
        newTradeRoute = new TradeRoute(sourceIsland, targetIsland, dict.id);
    } else if (dict.id !== undefined) {
        TradeRoute.deleteById(dict.id);
        realReturn = false;
    }
    if (dict._version === undefined || dict._version === 1) {
        newTradeRoute.addProduct(dict._product_id, dict._amount);
    } else {
        dict._products.forEach(x => newTradeRoute.addProduct(x.productID, x.Amount));
    }
    return realReturn ? newTradeRoute : undefined;
}
