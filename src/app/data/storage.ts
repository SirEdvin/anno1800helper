import LocalDB from 'local-db';
import { NgxIndexedDB } from './ngx-indexed-db';

export const legacyDB = new LocalDB('Base User');

export const db = new NgxIndexedDB('baseDB', 1);

const temporaryID = -200;


export abstract class DatabaseModel {
    _id: number;

    protected save() {
        const baseDict = this.to_dict();
        if (this._id === undefined) {
            this._id = temporaryID;
            db.add(this.storeName, baseDict).then(x => this._id = x);
        } else if (this._id !== temporaryID) {
            db.update(this.storeName, baseDict);
        }
    }

    public delete() {
        db.delete(this.storeName, this._id);
    }

    abstract get storeName();
    abstract to_dict();
}
