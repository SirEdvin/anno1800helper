import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { ConfirmDialogComponent } from '../dialogs/confirm-dialog.component';
import { EditFactoryDialogComponent } from '../dialogs/edit-factory-dialog.component';

import { Factory, FactoryState } from '../data/factories';


@Component({
  selector: 'app-factory',
  templateUrl: './factory.component.html',
  styleUrls: ['./factory.component.css']
})
export class FactoryComponent implements OnInit {

  @Input('factory') factory: Factory;

  @Output('removeEvent') removeEvent = new EventEmitter();

  constructor(public modalService: BsModalService) { }

  ngOnInit() {
  }

  modifyFactory() {
    this.modalService.show(EditFactoryDialogComponent, {
      initialState: {
        factory: this.factory
      }
    });
  }

  get countModificationBlock(): boolean {
    return this.factory.State === FactoryState.TRADE;
  }

  deleteFactory() {
    const self = this;
    this.modalService.show(ConfirmDialogComponent, {
      initialState: {
        title: `Do you really what to remove ${self.factory.Name}?`,
        cancelCallback: () => {},
        confirmCallback: () => self.removeEvent.emit()
      }
    });
  }

}
