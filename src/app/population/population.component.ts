import { Component, OnInit, Input } from '@angular/core';

import { PopulationLevel } from '../data/populations';

@Component({
  selector: 'app-population',
  templateUrl: './population.component.html',
  styleUrls: ['./population.component.css']
})
export class PopulationComponent implements OnInit {

  @Input('population') population: PopulationLevel;
  @Input('houseDelta') houseDelta: number;

  constructor() { }

  ngOnInit() {
  }

  addHoused() {
    this.population.HouseCount += this.houseDelta;
  }

  removeHouses() {
    this.population.HouseCount -= this.houseDelta;
  }

  promoteHoused() {
    if (this.population.promotionSource !== undefined && this.population.promotionSource.HouseCount >= this.houseDelta) {
      this.population.promotionSource.HouseCount -= this.houseDelta;
      this.population.HouseCount += this.houseDelta;
    }
  }

  discourageeHouses() {
    if (this.population.promotionSource !== undefined && this.population.HouseCount >= this.houseDelta) {
      this.population.promotionSource.HouseCount += this.houseDelta;
      this.population.HouseCount -= this.houseDelta;
    }
  }

  get promoteMode() {
    return this.population.promotionSource !== undefined;
  }

}
