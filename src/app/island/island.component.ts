import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { ConfirmDialogComponent } from '../dialogs/confirm-dialog.component';
import { EditIslandDialogComponent } from '../dialogs/edit-island-dialog.component';
import { AddFactoryDialogComponent } from '../dialogs/add-factory-dialog.component';

import { Factory } from '../data/factories';
import { Island } from '../data/islands';


const layerBasedWidth = {
  0: 'w-100',
  1: 'w-50',
  2: 'w-32',
  3: 'w-25'
};


@Component({
  selector: 'app-island',
  templateUrl: './island.component.html',
  styleUrls: ['./island.component.css']
})
export class IslandComponent {

  @Input('island') island: Island;
  @Output('removeEvent') removeEvent = new EventEmitter();

  constructor(private modalService: BsModalService) { }

  removeFactory(factory: Factory) {
    this.island.removeFactory(factory);
  }

  get layerBasedWidth() {
    return layerBasedWidth[this.island.maxLayer];
  }

  addOldWorldFactory() {
    const self = this;
    this.modalService.show(AddFactoryDialogComponent, {
      initialState: {
        isOldWorldIsland: this.island.isOldWorld,
        isOldWorldFactory: true,
        factoryCallback: function (newFactory: Factory) {
          self.island.addFactory(newFactory);
        }
      }
    });
  }

  addNewWorldFactory() {
    const self = this;
    this.modalService.show(AddFactoryDialogComponent, {
      initialState: {
        isOldWorldIsland: this.island.isOldWorld,
        isOldWorldFactory: false,
        factoryCallback: function (newFactory: Factory) {
          self.island.addFactory(newFactory);
        }
      }
    });
  }

  editIsland() {
    this.modalService.show(EditIslandDialogComponent, {
      initialState: {
        island: this.island
      }
    });
  }

  removeIsland() {
    const self = this;
    this.modalService.show(ConfirmDialogComponent, {
      initialState: {
        title: `Do you really what to remove ${self.island.Name}?`,
        cancelCallback: () => {},
        confirmCallback: () => self.removeEvent.emit()
      }
    });
  }

}
