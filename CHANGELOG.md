# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.6.2] - 2019-05-26

### Added

- Strange ability to remove product from trade route

## [0.6.1] - 2019-05-24

### Added

- Loading progress bar
- Default sotring for islands and trade routes

## [0.6.0] - 2019-05-24

### Added

- Factory and population images
- Ability to remove island

### Changed

- Trade routes should be unique between islands
- Migrate from angular material to bootstap for island, factory, population and trade routes
- Replace angular-material with ngx-bootstrap

### Fixed

- Problem with trade route save

### Removed

- Promote toggling due to performance issue

## [0.5.1] - 2019-05-21

### Fixed

- Save/load logic now pass id

## [0.5.0] - 2019-05-21

### Added

- Confirmation before reset game
- Save/Load game feature

## [0.4.0] - 2019-05-19

### Added

- `Reset Game` button

### Changed

- Migrate from localStorage usage to indexedDB usage
